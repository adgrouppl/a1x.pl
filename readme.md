# Landing Page

## Prerequisites

In order to run the application you need to setup server with:

- PHP 7+
- MySQL 5.7+

For development process, you should use [Laravel Homestead](https://laravel.com/docs/5.3/homestead)

## Coding standards

In order to follow code standards, you should use PSR-2, install [PHP CS Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) and before each commit you should run this fixer to make sure your code is properly formatted. Base CS fixer configuration file has created as `.php_cs` to make sure every developer uses the same rules for code formatting.

Line limit should be set to 100 characters. You should not exceed this limit in any PHP file (except Blade files)

## Installation

1. Copy `.env.example` as `.env`

2. Run `composer install`

3. In `.env` file:

    - Set `APP_KEY` to random 32 characters long string using the following command:
    
    ```
    php artisan key:generate
    ```
    
4. Run
 
    ```
    php artisan migrate
    ```
    
    to run migrations into database  
    
## Running tests

**You should always run tests in separate directory (you should clone application into separate directory for running tests) and with database connection different that database connection you use for application to make sure running tests won't ruin existing data (both database and files)** 

In order to run `phpunit` tests you should prepare a few things.

1. You need to create separate database connection and set it in `.env` file in `TESTING` section (you can leave default database connection data not filled - they won't be used when running tests)
 
2. Run `php artisan migrate --database=mysql_testing` to run all migrations into this testing database (don't change `mysql_testing` when running this command - it's not the database name!)

3. You should fill in all `.env` settings (except standard database connection). You should make sure that in your `.env` file `MAIL_DRIVER` is set to `log` (it's set like this by default in `phpunit.xml`.

4. You need to make sure you have set `APP_DEBUG` in `.env` file to `false`.

After configuration you should be able to run tests using `./vendor/bin/phpunit` command.